// SPDX-License-Identifier: MIT
pragma solidity ^0.8;

contract StakingPool {
    IERC20 public rewardsToken;
    IERC20 public stakingToken;

    address public owner;

    uint public lastUpdateTime;
    uint public totalSupply;
    uint public freezeTime;
    uint public rewardCoef;

    mapping(address => uint) public userRewardPerTokenPaid;
    mapping(address => uint) public rewards;
    mapping(address => uint) private _balances;
    mapping(address => uint) private _updates;

    constructor(address _stakingToken, address _rewardsToken) {
        owner = msg.sender;
        freezeTime = 20 minutes;
        rewardCoef = 5;
        stakingToken = IERC20(_stakingToken);
        rewardsToken = IERC20(_rewardsToken);
    }

    modifier onlyOwner() {
        require(msg.sender == owner, "Must be the owner");
        _;
    }

    modifier updateReward(address account) {
        rewards[account] = _balances[account] / rewardCoef;
        _;
    }

    function stake(uint _amount) external updateReward(msg.sender) {
        totalSupply += _amount;
        _balances[msg.sender] += _amount;
        _updates[msg.sender] = block.timestamp;
        stakingToken.transferFrom(msg.sender, address(this), _amount);
    }

    function unstake(uint _amount) external updateReward(msg.sender) {
        require(_updates[msg.sender] >= block.timestamp + freezeTime, "Too early");
        require(totalSupply >= _amount, "Not enough tokens");
        totalSupply -= _amount;
        _balances[msg.sender] -= _amount;
        stakingToken.transfer(msg.sender, _amount);
    }

    function claim() external {
        uint reward = rewards[msg.sender];
        rewards[msg.sender] = 0;
        rewardsToken.transfer(msg.sender, reward);
    }

    function showReward(address account) external view returns (uint) {
        return rewards[account];
    }

    function setFreezeTime(uint time) external onlyOwner {
        freezeTime = time;
    }

    function setRewardCoef(uint coefficient) external onlyOwner {
        rewardCoef = coefficient;
    }
}

interface IERC20 {
    function totalSupply() external view returns (uint);

    function balanceOf(address account) external view returns (uint);

    function transfer(address recipient, uint amount) external returns (bool);

    function allowance(address owner, address spender) external view returns (uint);

    function approve(address spender, uint amount) external returns (bool);

    function transferFrom(
        address sender,
        address recipient,
        uint amount
    ) external returns (bool);

    event Transfer(address indexed from, address indexed to, uint value);
    event Approval(address indexed owner, address indexed spender, uint value);
}