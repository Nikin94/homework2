const { ethers } = require('hardhat')

async function main() {
  const [deployer] = await ethers.getSigners()
  console.log(`Deploying contracts with the account: ${deployer.address}`)

  const balance = await deployer.getBalance()
  console.log(`Account balance: ${balance.toString()}`)

  const Pool = await ethers.getContractFactory('Pool')
  const pool = await Pool.deploy(
    '0x5FbDB2315678afecb367f032d93F642f64180aa3',
    '0x5FbDB2315678afecb367f032d93F642f64180aa3'
  )
  console.log(`Contract address: ${pool.address}`)
}

main().catch(error => {
  console.error(error)
  process.exitCode = 1
})
