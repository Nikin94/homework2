import { expect } from 'chai'
import { ethers, network } from 'hardhat'
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers'
import { StakingPool } from '../typechain'

let pool: StakingPool
let owner: SignerWithAddress
let addr1: SignerWithAddress
let addr2: SignerWithAddress

beforeEach(async function () {
  ;[owner, addr1, addr2] = await ethers.getSigners()

  const Pool = await ethers.getContractFactory('StakingPool')
  pool = await Pool.deploy(
    '0x5FbDB2315678afecb367f032d93F642f64180aa3',
    '0x5FbDB2315678afecb367f032d93F642f64180aa3'
  )
  await pool.deployed()
})

describe('StakingPool', function () {
  it('Should set the right owner', async () => {
    expect(await pool.owner()).to.equal(owner.address)
  })

  it('Should fail if too early - unstake', async () => {
    await expect(pool.unstake(99)).to.be.revertedWith('Too early')
  })

  it('Should fail if not enough tokens - unstake', async () => {
    await network.provider.send('evm_increaseTime', [3600])
    await network.provider.send('evm_mine')
    await expect(pool.unstake(99)).to.be.revertedWith('Not enough tokens')
  })
  it('Should stake', async () => {
    const totalSupply = await pool.totalSupply()
    const balance = await owner.getBalance()
    await pool.stake(99)
    expect(await owner.getBalance()).to.equal(balance.sub(99))
    expect(await pool.totalSupply()).to.equal(totalSupply.add(99))
  })

  it('Should unstake', async () => {
    const balance = await owner.getBalance()
    await network.provider.send('evm_setNextBlockTimestamp', [1655097600])
    await network.provider.send('evm_mine')
    await pool.stake(100)
    await pool.unstake(20)
    expect(await owner.getBalance()).to.equal(balance.sub(80))
  })

  it('Should claim', async () => {
    const reward = await pool.showReward(owner.address)
    const balance = await owner.getBalance()
    await pool.claim()
    expect(await owner.getBalance()).to.equal(balance.sub(reward))
  })

  it('Should set freeze time', async () => {
    await pool.setFreezeTime(100)
    expect(await pool.freezeTime()).to.equal(100)
  })

  it('Should fail if not owner', async () => {
    await expect(pool.connect(addr2).setFreezeTime(100)).to.be.revertedWith(
      'Must be the owner'
    )
  })

  it('Should set reward coefficient', async () => {
    await pool.setRewardCoef(11)
    expect(await pool.rewardCoef()).to.equal(11)
  })
})
