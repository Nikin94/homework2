import { ethers } from 'hardhat'
import { task } from 'hardhat/config'

task('unstake', 'Unstake')
  .addParam('address', "The contract's address")
  .addParam('amount', 'Amount of tokens')
  .setAction(async ({ address, amount }) => {
    const Contract = await ethers.getContractFactory('StakingPool')
    const contract = await Contract.attach(address)
    const unstake = await contract.unstake(amount)
    console.log(unstake)
  })
