import { ethers } from 'hardhat'
import { task } from 'hardhat/config'

task('stake', 'Stake')
  .addParam('address', "The contract's address")
  .addParam('amount', 'Amount of tokens')
  .setAction(async ({ address, amount }) => {
    const Contract = await ethers.getContractFactory('StakingPool')
    const contract = await Contract.attach(address)
    const stake = await contract.stake(amount)
    console.log(stake)
  })
