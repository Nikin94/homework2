import { ethers } from 'hardhat'
import { task } from 'hardhat/config'

task('claim', 'Claim')
  .addParam('address', "The contract's address")
  .setAction(async ({ address }) => {
    const Contract = await ethers.getContractFactory('StakingPool')
    const contract = await Contract.attach(address)
    const claim = await contract.claim()
    console.log(claim)
  })
